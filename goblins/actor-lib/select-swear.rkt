#lang racket

;;; Copyright 2019-2020 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(require "../core.rkt")

(provide select-$/<-
         run-$/<-)

;; A helper to select $ or <-.
;; Combined they look like a cartoon character swearing.
;; Probably a better name is warranted.
;; TODO: This isn't really correct, because there are some
;;   near-refrs, such as promises, which can't be called with $
(define (select-$/<- to-refr)
  (if (near-refr? to-refr)
      $ <-))

(define run-$/<-
  (make-keyword-procedure
   (lambda (kws kw-args to-refr . args)
     (keyword-apply (select-$/<- to-refr)
                    kws kw-args to-refr args))))

(module+ test
  (require "../vat.rkt"
           rackunit)
  (define vat-a (make-vat))
  (define vat-b (make-vat))
  (define ((^simple-robot bcom))
    'beep-boop)
  (define robot-a
    (vat-a 'spawn ^simple-robot))
  (define robot-b
    (vat-b 'spawn ^simple-robot))
  
  (define ((^swear-selector bcom) to)
    (select-$/<- to))

  (define swear-selector-a
    (vat-a 'spawn ^swear-selector))

  (test-eq?
   "select-$/<- to object on same vat gets $"
   (vat-a 'call swear-selector-a robot-a)
   $)

  (test-eq?
   "select-$/<- to object on remote vat gets <-"
   (vat-a 'call swear-selector-a robot-b)
   <-))
